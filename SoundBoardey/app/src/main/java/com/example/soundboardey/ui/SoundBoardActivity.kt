package com.example.soundboardey.ui

import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import com.example.soundboardey.databinding.ActivitySoundboardBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class SoundBoardActivity : AppCompatActivity() {
    private val viewModel by viewModel<PersonViewModel>()
    private lateinit var binding: ActivitySoundboardBinding
    private lateinit var imgBtns: List<ImageButton>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySoundboardBinding.inflate(layoutInflater).also {
            setContentView(it.root)
            imgBtns = listOf(
                it.imgBtnElonM,
                it.imgBtnMarkZ,
                it.imgBtnZdravkoM
            )
            imgBtns.forEach { it.setOnClickListener { playSound(it) } }
        }
    }

    private fun playSound(imageButton: View) {
        viewModel.playPersonSound(resources.getResourceEntryName(imageButton.id))
    }
}